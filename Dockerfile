FROM nginx:alpine
# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html
# Remove default nginx static assets
RUN rm -rf ./*
# Copy static assets from builder stage
COPY build .
# change file nginx.conf to my file nginx.conf
RUN rm -rf /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx/
# Containers run nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]
