#!/bin/bash
docker_deploy=`cd /home/gitlab-runner/$CI_PROJECT_NAME && sudo docker-compose up -d --build`
docker_redeploy=`cd /home/gitlab-runner/$CI_PROJECT_NAME && sudo docker compose up -d --build --remove-orphans`

echo "========================"
if $($docker_deploy); then
    echo "contenedor desplegado";
else
    echo "-------------El contenedor no subió----------------";
    $docker_redeploy;
   echo "--------contenedor redesplegado--------";
fi;
